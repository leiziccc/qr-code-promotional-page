const bgm = uni.createInnerAudioContext();
bgm.src = 'https://public-read-shengmu.oss-cn-beijing.aliyuncs.com/image/bg.mp3'
// bgm.src = 'https://bjetxgzv.cdn.bspapp.com/VKCEYUGU-hello-uniapp/2cc220e0-c27a-11ea-9dfb-6da8e309e0d8.mp3'



bgm.loop = true;  //循环播放
bgm.autoplay=true;  //自动播放
bgm.sessionCategory = 'soloAmbient'; //在后台时不播放，如有其他播放，会暂停其他播放（但在移动端h5中 后台不播放失效）（其他属性自行百度）
bgm.volume = 0.4  //音量



console.log(bgm,77777777777);

var music = {
	//mute 表示是否是静音，，默认静音
	playBgm({
		mute = true
	}) {
		if (!bgm) return;
		if (mute) {
			bgm.pause()
		} else {
			bgm.play()
		}
 
		bgm.onPause(() => {})
		bgm.onPlay(() => {})
		bgm.onError((res) => {})
	}
}
module.exports = music